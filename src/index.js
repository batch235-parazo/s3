const express = require("express");
const app = express();
const PORT = 4000;

app.use(express.json());

let users = [
	{
		name: "Jojo Joestar",
		age: 25,
		username: "Jojo"
	},
	{
		name: "Dio Brando",
		age: 23,
		username: "Dio"
	},
	{
		name: "Jotaro Kujo",
		age: 28,
		username: "Jotaro"
	}
]

// [SECTION] Routes and Controllers for USERS

app.get("/users", (req, res) => {
	return res.send(users);
});


app.post("/users", (req, res) => {

	// add simple if statement that if the request body does not have property name, we will send message along with a 400 http status code (Bad Request)

	//hasOwnProperty() returns a boolean if the property name passed exists or does not exist in the given object

	if(!req.body.hasOwnProperty("name")) {
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}
})

app.post("/users", (req, res) => {

	if(!req.body.hasOwnProperty("age")) {
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}
})

// Activity - Artists

let artists = [
	{
		name: "Earth, Wind, & Fire",
		songs: [
			"September",
			"Let's Groove"
			],
		album: "The Eternal Dance",
		isActive: true
	},
	{
		name: "Dallas String Quartet",
		songs: [
			"Don't Stop Believin'",
			"Firework"
			],
		album: "Red",
		isActive: true
	},
	{
		name: "Toploader",
		songs: [
			"Dancing in the Moonlight",
			"Achilles Heel"
			],
		album: "The Best of Toploader",
		isActive: true
	}
]


// [SECTION] Routes and Controllers for ARTISTS

app.get("/artists", (req, res) => {
	return res.send(artists);
});


app.post("/artists", (req, res) => {


	if(!req.body.hasOwnProperty("name")) {
		return res.status(400).send({
			error: "Bad Request - missing required parameter NAME"
		})
	}


	if(!req.body.hasOwnProperty("songs")) {
		return res.status(400).send({
			error: "Bad Request - missing required parameter SONGS"
		})
	}


	if(!req.body.hasOwnProperty("album")) {
		return res.status(400).send({
			error: "Bad Request - missing required parameter ALBUM"
		})
	}


	if(!req.body.hasOwnProperty("isActive")) {
        return res.status(400).send({
            error: "Bad Request - missing required parameter ISACTIVE"
        })

    }

    if(req.body.isActive === false) {
        return res.status(400).send({
            error: "Bad Request - user isActive status is false"
        })

    }
});





app.listen(PORT, () => console.log(`Running on port ${PORT}`));







